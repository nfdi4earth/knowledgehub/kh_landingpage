# KnowledgeHub Landing Page

This project administrates the central landing page of the NFDI4Earth Knowledge Hub.

It's built up from a very [simple exemplary Bootstrap 4.0 theme](https://getbootstrap.com/docs/4.0/examples/pricing/).

## Deployment

This project uses CI/CD pipelines to automatically deploy the latest state to
the virtual machines of NFDI4Earth Knowledgehub.

### VM-preparation

The VM needs to prepared to be able to get accessed via CI/CD-pipeline. The script [`prepare4pipeline.sh`](prepare4pipeline.sh) contains all the necessary steps:

- create dedicated user 'user4lp'
- prepare ssh for that user
- set permissions and ownership of `/var/www` to enable 'user4lp' to handle git project
- clone this git project to `/var/www` and adapt permissions/ownership

> Note: The public key is stored as secret variable within the GitLab-CI/CD-settings and needs to be added manually!

### Run pipeline

When the VM is set up well, the pipeline can run as defined in: [`.gitlab-ci.yml`](.gitlab-ci.yml).

> Info: This script is fundamentally based on a very good [tutorial](https://www.programonaut.com/how-to-deploy-a-git-repository-to-a-server-using-gitlab-ci-cd/)!

Basically, the integrated tasks are identical for both stages. Only ssh_host and git branches are different.

Short wrap up of what happens:

1. prepare ssh (within the docker container) to enable access to virtual machine
2.  - connect to virtual machine
    - go to workdir of landing page
    - discard local changes
    - checkout corresponding branch
    - pull updates
    - adapt ownership of workdir
    - exit VM
3. remove ssh setting (within docker container)

## Branches:

Currently, we develop on 2 main branches:

- master: used to develop and stage the project
    
    -> commits get deployed to the [Test-Instance](https://knowledgehub.test.n4e.geo.tu-dresden.de)

- production: used to maintain the project
    
    -> commits get deployed to the [Production-Instance](https://knowledgehub.nfdi4earth.de)

## ToDo:


## License

This project is published under the Apache License 2.0, see file LICENSE.
Contributors: Ralf Klammer