# !!!The script has to be executed by the root user!!!
# This script is used to prepare the VM for the CI/CD pipeline
# It creates a new user, adds the public key to the authorized_keys file
# and clones the repository to the server
# The setup of the CI/CD pipeline is pretty much based on following tutorial:
# https://www.programonaut.com/how-to-deploy-a-git-repository-to-a-server-using-gitlab-ci-cd/

export USERNAME=user4lp
export PUBKEY="ssh-rsa {{public key can be found in CI/CD variables: SSH_PUB_KEY}}"
export WORKDIR="/var/www/kh_landingpage"

useradd -m $USERNAME
mkdir /home/$USERNAME/.ssh
echo $PUBKEY > /home/$USERNAME/.ssh/authorized_keys
chown $USERNAME:users -R /home/$USERNAME/.ssh

adduser $USERNAME www-data
chown root:www-data /var/www 
chmod 757 /var/www
rm -r $WORKDIR
git clone https://git.rwth-aachen.de/nfdi4earth/knowledgehub/kh_landingpage.git $WORKDIR
chown $USERNAME:www-data -R $WORKDIR
chmod 750 -R $WORKDIR
