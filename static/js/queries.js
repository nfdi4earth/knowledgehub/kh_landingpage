var queries = [{
            title: "Services in the NFDI4Earth",
            query: `PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX org: <http://www.w3.org/ns/org#>

SELECT ?serviceName ?type (GROUP_CONCAT(?NFDI4EarthPublisher;separator=";") AS ?publishers)
# GROUP_CONCAT is required to prevent multiple result rows due to organizations having names in multiple languages
WHERE {
  ?nfdi4earthProject foaf:homepage <https://nfdi4earth.de/> .
  ?organizationIRI org:hasMembership ?membership .
  ?membership org:organization ?nfdi4earthProject .
  {
    ?organizationIRI foaf:name ?NFDI4EarthPublisher .
    ?resource dct:publisher ?organizationIRI .
  }UNION {
    # In the KH, sometimes also the suborganization of a direct NFDI4Earth member organization
    # is responsible for a service, therefore we need this additional UNION statement
    ?subOrg org:subOrganizationOf ?organizationIRI .
    ?subOrg foaf:name ?NFDI4EarthPublisher .
    FILTER NOT EXISTS {
      ?resource dct:publisher ?anyPublisher .
      ?anyPublisher org:hasMembership ?membership2 .
      ?membership2 org:organization ?nfdi4earthProject
    }
    ?resource dct:publisher ?subOrg .
  }
  ?resource dct:title ?serviceName .
  ?resource a ?type .
  ?resource n4e:sourceSystem ?sourceSystem .
  ?sourceSystem dct:title ?sourceSystemTitle .
} GROUP BY ?serviceName ?type
        `,
            description: "Query all services which are run by an organization which is part of NFDI4Earth",
        },
        {
            title: "Metadata",
            query: `PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT ?repositoryName ?standardName ?apiType
WHERE {
  ?standard a n4e:MetadataStandard .
  ?standard dct:title ?standardName .
  ?repo a n4e:Repository .
  ?repo dct:title ?repositoryName .
  ?repo n4e:supportsMetadataStandard ?standard .
  ?repo n4e:hasAPI ?api .
  ?api dct:conformsTo ?apiType
}
      `,
            description: "MetadataStandards and APIs supported by repositories",
        },
        {
            title: "Educational resources",
            query: `PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX schema: <http://schema.org/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

CONSTRUCT {
  ?resource ?predicate ?object .
  ?object ?otherPredicate ?otherObject  .
}
WHERE {
  ?resource a schema:LearningResource .
  ?resource ?predicate ?object .
  OPTIONAL {
    ?resource schema:publisher ?object .
    BIND(foaf:name as ?otherPredicate) .
    ?object ?otherPredicate ?otherObject
  }
  OPTIONAL {
    ?resource schema:about ?object .
    BIND(dct:title as ?otherPredicate) .
    ?object dct:title ?object2
  }
  OPTIONAL {
    ?resource schema:license ?object .
    BIND(<http://spdx.org/rdf/terms#licenseId>  as ?otherPredicate) .
    SERVICE <https://sparql.knowledgehub.nfdi4earth.de/spdx/sparql> {
      ?object ?otherPredicate ?otherObject
    }
  }
  OPTIONAL {
    ?resource <https://nfdi.fiz-karlsruhe.de/ontology/subjectArea> ?object .
    BIND(rdfs:label as ?otherPredicate) .
    SERVICE <https://sparql.knowledgehub.nfdi4earth.de/dfgfo/sparql> {
      ?object rdfs:label ?otherObject
    }
  }
}
      `,
            description: "All information on entities of type LearningResource in the Knowledge Hub",
        },
    ],
    sparql_url = "https://sparql.knowledgehub.nfdi4earth.de",
    sub_url = "/knowledge-graph/query?query=";