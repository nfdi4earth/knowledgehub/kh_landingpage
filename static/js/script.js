const sparql_url = "https://sparql.knowledgehub.nfdi4earth.de",
    sub_url = "/knowledge-graph/query?query=";

var renderCard = function (data) {
    let description_parts = data.response
        .split('\n')
        .slice(0, 3)
        .filter(function (line) {
            return line.startsWith('#');
        });
    let description = description_parts
        .join('\n')
        .replace('# ', '')
        .replace('\n# ', '\n')
        .replace('#', '');
    let try_url = sparql_url + "/#/dataset" + sub_url + encodeURIComponent(data.response),
        data_url = sparql_url + sub_url + encodeURIComponent(data.response),
        card = $("#card-template > div").clone();

    if (data && data.title) card.find(".card-header #card-header").text(data.title);
    else card.find(".card-header").hide();

    card.find("p.card-text").text(description);
    card.find("a.download").each(function (i, d) {
        var self = $(this);
        self.attr("href", data_url + "&output=" + self.data("type"));
    });
    card.find("a.card-button").attr("href", try_url);

    card.appendTo(data.card_deck);
};

var fetchGitLab = function (data) {
    fetch(data.url, { headers: { 'PRIVATE-TOKEN': 'glpat-5Fy-zjKzXW2wWojz9Rab' } })
        .then(function (response) {
            if (data != undefined && data.datatype === 'json') return response.json();
            else return response.text();
        })
        .then(function (response) {
            data.response = response;
            data.callback(data)
        })
        .catch(function (err) {
            // There was an error
            console.warn('Something went wrong.', err);
        });
}

var getQuestionaireQuery = function (filename) {
    fetchGitLab({
        callback: renderCard,
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/files/queries%2F' + filename + '/raw?ref=main',
        datatype: 'text',
        card_deck: $("#hidden-card-deck .row")
    })
};
var getQuestionaireList = function () {
    fetchGitLab({
        callback: function (data) {
            data.response.forEach(function (elem) {
                if (elem.type === 'blob') getQuestionaireQuery(elem.name);
            });
            // remove event from DOM, that triggers this function
            $('#hidden-card-deck').off('show.bs.collapse')
        },
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/tree?ref=main&path=queries',
        datatype: 'json'
    })
};

var getExampleQuery = function (filename) {
    fetchGitLab({
        callback: renderCard,
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/files/examples%2F' + filename + '/raw?ref=main',
        datatype: 'text',
        title: filename.replace('.rq', ''),
        card_deck: $("#card-deck")
    })
};
var getExamplesList = function () {
    fetchGitLab({
        callback: function (data) {
            data.response.forEach(function (elem) {
                if (elem.type === 'blob') getExampleQuery(elem.name);
            })
        },
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/tree?ref=main&path=examples',
        datatype: 'json'
    })
};

// page is fully loaded, so functions can be executed now
$(function () {
    getExamplesList();

    $('#hidden-card-deck').on('show.bs.collapse', function () {
        getQuestionaireList();
    })

    console.log("script.js ready!");
});